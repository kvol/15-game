﻿using System;
using UnityEngine;
namespace Utilities.Direction
{
    public static class DirectionExtension
    {
        public static Vector2Int GetVector(this Direction direction)
        {
            Vector2Int vector;
            switch (direction)
            {
                case Direction.Bottom:
                    vector = new Vector2Int(0, -1);
                    break;
                case Direction.Top:
                    vector = new Vector2Int(0, 1);
                    break;
                case Direction.Left:
                    vector = new Vector2Int(-1, 0);
                    break;
                case Direction.Right:
                    vector = new Vector2Int(1, 0);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
            }

            return vector;
        }
    }
}