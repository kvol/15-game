﻿using System;
using UnityEngine;

namespace Utilities
{
    public static class Vector2IntExtension
    {
        public static Vector2Int Normalize(this Vector2Int vector)
        {
            if (vector.x != 0)
            {
                vector.x /= Math.Abs(vector.x);
            }

            if (vector.y != 0)
            {
                vector.y /= Math.Abs(vector.y);
            }

            return vector;
        }
    }
}