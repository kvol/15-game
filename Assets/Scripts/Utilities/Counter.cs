﻿using Utilities.Properties;

namespace Utilities
{
    public class Counter
    {
        public IntProperty Count { get; } = new IntProperty();

        public void Tick()
        {
            Count.Value++;
        }

        public void Reset()
        {
            Count.Value = 0;
        }
    }
}