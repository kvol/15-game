﻿using UnityEngine;

namespace Utilities
{
    public struct MoveVector2Int
    {
        public Vector2Int from;
        public Vector2Int to;

        public MoveVector2Int(Vector2Int from, Vector2Int to)
        {
            this.from = from;
            this.to = to;
        }

        public override string ToString()
        {
            return $"({from} : {to})";
        }
    }
}