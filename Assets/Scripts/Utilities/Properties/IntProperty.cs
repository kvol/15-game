﻿using System;
using Utilities.Properties.Interfaces;

namespace Utilities.Properties
{
    [Serializable]
    public class IntProperty : IProperty
    {
        private int _value;

        public event Action<string> ValueChanged;

        public IntProperty()
        {
            _value = 0;
        }

        public IntProperty(int value)
        {
            _value = value;
        }

        public int Value
        {
            get => _value;
            set
            {
                _value = value;
                ValueChanged?.Invoke($"{this._value}");
            }
        }
    }
}