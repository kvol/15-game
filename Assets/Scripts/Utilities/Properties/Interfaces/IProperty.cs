﻿using System;

namespace Utilities.Properties.Interfaces
{
    public interface IProperty
    {
        event Action<string> ValueChanged;
    }
}