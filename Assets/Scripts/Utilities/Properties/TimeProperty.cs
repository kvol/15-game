﻿using System;
using Utilities.Properties.Interfaces;

namespace Utilities.Properties
{
    public class Time : IProperty, ICloneable
    {
        private int _minutes;
        private int _seconds;

        public event Action<string> ValueChanged;

        public int Minutes
        {
            get => _minutes;
            set => _minutes = value;
        }

        public int Seconds
        {
            get => _seconds;
            set
            {
                _seconds = value;
                UpdateState();
            }
        }

        public int TimeInSeconds
        {
            get => _minutes * 60 + _seconds;
            set
            {
                _seconds = value;
                _minutes = 0;
                UpdateState();
            }
        }

        public Time(int minutes, int seconds)
        {
            _minutes = minutes;
            _seconds = seconds;
            UpdateState();
        }


        private void UpdateState()
        {
            _minutes += _seconds / 60;
            _seconds %= 60;
            ValueChanged?.Invoke(ToString());
        }

        public override string ToString()
        {
            return $"{_minutes:D2}:{_seconds:D2}";
        }

        public object Clone()
        {
            return new Time(_minutes, _seconds);
        }
    }
}