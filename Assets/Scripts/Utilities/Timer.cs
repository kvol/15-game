﻿using Time = Utilities.Properties.Time;

namespace Utilities
{
    public class Timer
    {
        private float _milliseconds;

        public Time Time { get; }

        public Timer()
        {
            Time = new Time(0, 0);
        }

        public void Tick(float milliseconds)
        {
            _milliseconds += milliseconds;
            if (_milliseconds > 1000)
            {
                Time.Seconds++;
                _milliseconds -= 1000;
            }
        }

        public void Reset()
        {
            Time.Minutes = 0;
            Time.Seconds = 0;
        }
    }
}