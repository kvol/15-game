﻿using System;
using System.Collections.Generic;

namespace Model.Data
{
    [Serializable]
    public class GameRecordsData
    {
        public int CurrentTime { get; }

        public int CurrentMoves { get; }

        public List<int> RecordTime { get; }

        public List<int> RecordMoves { get; }

        public GameRecordsData(int currentTime, int currentMoves, List<int> recordTime,
            List<int> recordMoves)
        {
            CurrentTime = currentTime;
            CurrentMoves = currentMoves;
            RecordTime = recordTime;
            RecordMoves = recordMoves;
        }
    }
}