﻿using System;

namespace Model.Data
{
    [Serializable]
    public class GridStateData
    {
        private readonly int[,] _grid;

        public int GridSize { get; }
        public int[,] Grid
        {
            get
            {
                int[,] grid = new int[GridSize, GridSize];
                Array.Copy(_grid, grid, GridSize * GridSize);
                return grid;
            }
        }

        public GridStateData(int[,] grid, int gridSize)
        {
            _grid = new int[gridSize, gridSize];
            Array.Copy(grid, _grid, gridSize * gridSize);
            GridSize = gridSize;
        }
    }
}