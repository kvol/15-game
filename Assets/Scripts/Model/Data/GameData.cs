﻿using System;

namespace Model.Data
{
    [Serializable]
    public class GameData
    {
        public GridStateData GridStateData { get; }
        public GameRecordsData GameRecordsData { get; }
        public bool IsGameRunning { get; }

        public GameData(GridStateData gridStateData, GameRecordsData gameRecordsData, bool isGameRunning)
        {
            GridStateData = gridStateData;
            GameRecordsData = gameRecordsData;
            IsGameRunning = isGameRunning;
        }
    }
}