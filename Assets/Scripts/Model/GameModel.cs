﻿using System;
using System.Collections.Generic;
using Model.Data;
using Utilities;
using Utilities.Direction;
using Vector2Int = UnityEngine.Vector2Int;

namespace Model
{
    public class GameModel
    {
        private const int GapNumber = 0;
        private readonly int[,] _grid;
        private readonly int _gridSize;
        private Vector2Int _gapPosition;

        public GridStateData GameModelState => new GridStateData(_grid, _gridSize);

        public GameModel(int gridSize)
        {
            if (gridSize < 1)
            {
                throw new ArgumentException("Grid size can't be negative");
            }

            _gridSize = gridSize;
            _grid = new int[gridSize, gridSize];

            for (int y = 0; y < gridSize; y++)
            {
                for (int x = 0; x < gridSize; x++)
                {
                    int number = y * gridSize + x + 1;
                    _grid[y, x] = number;
                }
            }

            _grid[gridSize - 1, gridSize - 1] = GapNumber;
            _gapPosition = new Vector2Int(gridSize - 1, gridSize - 1);
        }

        public GameModel(GridStateData gridStateData)
        {
            _gridSize = gridStateData.GridSize;
            _grid = gridStateData.Grid;

            for (int y = 0; y < _gridSize; y++)
            {
                for (int x = 0; x < _gridSize; x++)
                {
                    if (_grid[y, x] == GapNumber)
                    {
                        _gapPosition = new Vector2Int(x, y);
                        return;
                    }
                }
            }

            throw new ArgumentException("No gap cell in this grid!");
        }

        public Queue<MoveVector2Int> TryMove(Vector2Int startPosition)
        {
            Queue<MoveVector2Int> movementList = new Queue<MoveVector2Int>();

            if (IsPointOutOfBounds(startPosition))
            {
                return movementList;
            }

            if (startPosition.x != _gapPosition.x && startPosition.y != _gapPosition.y)
            {
                return movementList;
            }

            Vector2Int movementDirection = ((_gapPosition - startPosition) * -1).Normalize();


            for (Vector2Int currentPosition = _gapPosition;
                currentPosition != startPosition;
                currentPosition += movementDirection)
            {
                Vector2Int from = currentPosition;
                Vector2Int to = currentPosition + movementDirection;
                SwapCells(from, to);
                movementList.Enqueue(new MoveVector2Int(from, to));
            }

            _gapPosition = startPosition;

            return movementList;
        }
        
        private void SwapCells(Vector2Int from, Vector2Int to)
        {
            (_grid[from.y, from.x], _grid[to.y, to.x]) = (_grid[to.y, to.x], _grid[from.y, from.x]);
        }

        public bool IsGridSolved()
        {
            for (int y = 0; y < _gridSize; y++)
            {
                for (int x = 0; x < _gridSize; x++)
                {
                    int expectedNumber = y * _gridSize + x + 1;
                    if (_grid[y, x] != expectedNumber)
                    {
                        int rightBottomCell = _gridSize - 1;
                        if (y != rightBottomCell || x != rightBottomCell || _grid[y, x] != GapNumber)
                        {
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public Queue<Queue<MoveVector2Int>> Shuffle(int times)
        {
            Queue<Queue<MoveVector2Int>> movesQueue = new Queue<Queue<MoveVector2Int>>();

            Random randomizer = new Random();

            Vector2Int excludedDirection = new Vector2Int(0, 0);

            for (int i = 0; i < times; i++)
            {
                Vector2Int direction = ((Direction) randomizer.Next(4)).GetVector();
                Vector2Int cellToMovePosition = _gapPosition - direction * randomizer.Next(1, _gridSize);
                direction *= direction;
                if (IsPointOutOfBounds(cellToMovePosition) || direction == excludedDirection)
                {
                    i--;
                    continue;
                }

                excludedDirection = direction;
                movesQueue.Enqueue(TryMove(cellToMovePosition));
            }

            return movesQueue;
        }

        private bool IsPointOutOfBounds(Vector2Int point)
        {
            return point.x < 0 || point.x >= _gridSize || point.y < 0 || point.y >= _gridSize;
        }
    }
}