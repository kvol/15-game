﻿using System;
using System.Collections;
using System.Collections.Generic;
using Model;
using Model.Data;
using UnityEngine;
using UnityEngine.UI;
using Utilities;
using View.Cells;

namespace View
{
    public class GameViewController : MonoBehaviour
    {
        [SerializeField] private NumberCell _numberCell;
        [SerializeField] private SpaceCell _spaceCell;
        [SerializeField] private GridLayoutGroup _gridLayoutGroup;
        [SerializeField, Range(0, 10.0f)] private float _defaultAnimationTime;
        [SerializeField, Range(0, 10.0f)] private float _shufflingAnimationTime;
        private float _animationTime;

        private int _gridSize;
        private GameViewCell[,] _cells = new GameViewCell[0, 0];

        private readonly Queue<Queue<MoveVector2Int>> _movementsQueue = new Queue<Queue<MoveVector2Int>>();

        private Coroutine _runningAnimation;

        private CellsMover _cellsMover;
        private CellsCreator _cellsCreator;

        public event Action<Vector2Int> CellClicked;
        public event Action AnimationEnded;

        public void Initialize(int gridSize)
        {
            ResetState();
            
            _animationTime = _defaultAnimationTime;
            _gridSize = gridSize;
            _cellsCreator = new CellsCreator(_gridLayoutGroup, _numberCell, _spaceCell);
            _cells = _cellsCreator.CreateCellsGrid(_gridSize);
            _cellsMover = new CellsMover(_cells);

            foreach (var cell in _cells)
            {
                cell.CellClicked += OnCellClicked;
            }
        }

        public void Initialize(GridStateData gridStateData)
        {
            _animationTime = _defaultAnimationTime;
            _gridSize = gridStateData.GridSize;
            _cellsCreator = new CellsCreator(_gridLayoutGroup, _numberCell, _spaceCell);
            _cells = _cellsCreator.CreateCellsGrid(gridStateData);
            _cellsMover = new CellsMover(_cells);

            foreach (var cell in _cells)
            {
                cell.CellClicked += OnCellClicked;
            }
        }

        private void OnCellClicked(GameViewCell cell)
        {
            CellClicked?.Invoke(cell.Position);
        }

        private IEnumerator MakeMove()
        {
            while (_movementsQueue.Count > 0)
            {
                Queue<MoveVector2Int> movementQueue = _movementsQueue.Dequeue();
                foreach (var moveVector in movementQueue)
                {
                    _cellsMover.MoveCell(moveVector.from, moveVector.to, _animationTime);
                }

                yield return new WaitForSeconds(_animationTime);
            }

            AnimationEnded?.Invoke();
            _runningAnimation = null;
        }

        public void AddMovementToQueue(Queue<MoveVector2Int> movementQueue)
        {
            _movementsQueue.Enqueue(movementQueue);
            if (_runningAnimation == null)
            {
                _runningAnimation = StartCoroutine(MakeMove());
            }
        }

        public void ShufflingAnimation(Queue<Queue<MoveVector2Int>> shuffleMovements)
        {
            _animationTime = _shufflingAnimationTime;

            foreach (var shuffleMove in shuffleMovements)
            {
                AddMovementToQueue(shuffleMove);
            }

            AnimationEnded += ReturnDefaultAnimationSpeed;
        }

        private void ReturnDefaultAnimationSpeed()
        {
            AnimationEnded -= ReturnDefaultAnimationSpeed;
            _animationTime = _defaultAnimationTime;
        }

        private void ResetState()
        {
            _movementsQueue.Clear();
            foreach (var cell in _cells)
            {
                cell.CellClicked -= OnCellClicked;
                cell.Destroy();
            }

            _cells = null;
        }
    }
}