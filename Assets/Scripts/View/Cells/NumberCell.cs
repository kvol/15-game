﻿using TMPro;
using UnityEngine;

namespace View.Cells
{
    public class NumberCell : GameViewCell
    {
        private int _numberInsideCell;
        [SerializeField] private TextMeshProUGUI _text;

        public override void Initialize(Vector2Int position, int number)
        {
            base.Initialize(position, number);
            _numberInsideCell = number;
            _text.text = $"{number}";
        }
    }
}