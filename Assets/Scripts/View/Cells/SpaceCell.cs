﻿using UnityEngine;

namespace View.Cells
{
    public class SpaceCell : GameViewCell
    {
        public override void Initialize(Vector2Int position, int number)
        {
            base.Initialize(position, number);
            _image.enabled = false;
        }
    }
}