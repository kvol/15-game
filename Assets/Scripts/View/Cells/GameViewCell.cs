﻿using System;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace View.Cells
{
    [RequireComponent(typeof(Image), typeof(RectTransform))]
    public abstract class GameViewCell : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        protected Image _image;
        private Vector2Int _position;
        private RectTransform _rectTransform;
        public event Action<GameViewCell> CellClicked;

        public Vector2Int Position => _position;

        public virtual void Initialize(Vector2Int position, int number)
        {
            _position = position;
            _rectTransform = GetComponent<RectTransform>();
            _image = GetComponent<Image>();
        }


        public void Move(Vector2Int position, float time)
        {
            float cellSize = _rectTransform.rect.width;

            Vector2 vectorFrom = _rectTransform.anchoredPosition;
            Vector2 movementDirection = (position - _position) * new Vector2Int(1, -1);
            Vector2 vectorTo = vectorFrom + movementDirection * cellSize;

            _rectTransform.DOAnchorPos(vectorTo, time);

            _position = position;
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            CellClicked?.Invoke(this);
        }

        public void OnPointerDown(PointerEventData eventData)
        {
        }

        public void Destroy()
        {
            Destroy(gameObject);
        }
    }
}