﻿using UnityEngine;
using View.Cells;

namespace View
{
    public class CellsMover
    {
        private readonly GameViewCell[,] _cells;

        public CellsMover(GameViewCell[,] cells)
        {
            _cells = cells;
        }

        public void MoveCell(Vector2Int from, Vector2Int to, float animationTime)
        {
            _cells[from.y, from.x].Move(to, animationTime);
            _cells[to.y, to.x].Move(from, animationTime);

            (_cells[from.y, from.x], _cells[to.y, to.x]) = (_cells[to.y, to.x], _cells[from.y, from.x]);
        }
    }
}