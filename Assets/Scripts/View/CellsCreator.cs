﻿using DG.Tweening;
using Model;
using Model.Data;
using UnityEngine;
using UnityEngine.UI;
using View.Cells;

namespace View
{
    public class CellsCreator
    {
        private readonly NumberCell _numberCellPrefab;
        private readonly SpaceCell _spaceCellPrefab;

        private readonly GridLayoutGroup _gridLayoutGroup;
        private readonly RectTransform _rectTransform;

        public CellsCreator(GridLayoutGroup gridLayoutGroup, NumberCell numberCell,
            SpaceCell spaceCell)
        {
            _gridLayoutGroup = gridLayoutGroup;
            _rectTransform = _gridLayoutGroup.GetComponent<RectTransform>();
            _numberCellPrefab = numberCell;
            _spaceCellPrefab = spaceCell;
        }

        public GameViewCell[,] CreateCellsGrid(int gridSize)
        {
            GameViewCell[,] grid = new GameViewCell[gridSize, gridSize];
            AdjustGridParameters(gridSize);

            _gridLayoutGroup.enabled = true;

            for (int number = 0; number < gridSize * gridSize - 1; number++)
            {
                int y = number / gridSize;
                int x = number % gridSize;
                grid[y, x] = Object.Instantiate(_numberCellPrefab, _rectTransform);
                grid[y, x].Initialize(new Vector2Int(x, y), number + 1);
            }

            Vector2Int spaceCellPosition = new Vector2Int(gridSize - 1, gridSize - 1);
            grid[spaceCellPosition.y, spaceCellPosition.x] = Object.Instantiate(_spaceCellPrefab, _rectTransform);
            grid[spaceCellPosition.y, spaceCellPosition.x].Initialize(spaceCellPosition, 0);

            LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
            DOVirtual.DelayedCall(0.1f, () => _gridLayoutGroup.enabled = false);
            return grid;
        }

        public GameViewCell[,] CreateCellsGrid(GridStateData gridStateData)
        {
            int gridSize = gridStateData.GridSize;
            int[,] gridModel = gridStateData.Grid;
            GameViewCell[,] grid = new GameViewCell[gridSize, gridSize];
            AdjustGridParameters(gridSize);

            _gridLayoutGroup.enabled = true;

            for (int y = 0; y < gridSize; y++)
            {
                for (int x = 0; x < gridSize; x++)
                {
                    int cellNumber = gridModel[y, x];

                    GameViewCell cellPrefab =
                        cellNumber == 0 ? (GameViewCell) _spaceCellPrefab : _numberCellPrefab;

                    grid[y, x] = Object.Instantiate(cellPrefab, _rectTransform);
                    grid[y, x].Initialize(new Vector2Int(x, y), cellNumber);
                }
            }

            LayoutRebuilder.ForceRebuildLayoutImmediate(_rectTransform);
            DOVirtual.DelayedCall(0.1f, () => _gridLayoutGroup.enabled = false);
            return grid;
        }

        private void AdjustGridParameters(int gridSize)
        {
            float freeWidth = _rectTransform.rect.width;
            float cellSize = freeWidth / gridSize;
            _gridLayoutGroup.cellSize = new Vector2(cellSize, cellSize);

            _gridLayoutGroup.constraintCount = gridSize;
        }
    }
}