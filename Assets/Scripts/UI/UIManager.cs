﻿using System;
using System.Collections.Generic;
using Core.Interfaces;
using UI.Windows;
using UnityEngine;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
        [SerializeField] private StartWindow _startWindow;
        [SerializeField] private PlayingWindow _playWindow;
        [SerializeField] private VictoryWindow _victoryWindow;

        private List<WindowBase> _windowBaseList;

        private bool _playerHasControl;

        private readonly Dictionary<WindowType, WindowBase> _openedWindows = new Dictionary<WindowType, WindowBase>();

        public bool PlayerHasControl
        {
            get => _playerHasControl;
            set
            {
                if (value)
                {
                    foreach (var window in _openedWindows)
                    {
                        window.Value.SubscribeAllActions();
                    }
                }
                else
                {
                    foreach (var window in _openedWindows)
                    {
                        window.Value.UnsubscribeAllActions();
                    }
                }

                _playerHasControl = value;
            }
        }

        public void Initialize(Action startNewGame, Action<int> createNewGame, IRecordsProperties recordsProperties)
        {
            _windowBaseList = new List<WindowBase>
            {
                _startWindow,
                _playWindow,
                _victoryWindow
            };
            OpenWindow(WindowType.Game);

            _playWindow.Initialize(startNewGame, recordsProperties);
            _victoryWindow.Initialize(startNewGame, recordsProperties);
            _startWindow.Initialize(createNewGame);
        }

        public void OpenWindow(WindowType windowType)
        {
            _openedWindows.Add(windowType, _windowBaseList[(int) windowType]);
            _openedWindows[windowType].Open();
        }

        public void CloseAllWindows()
        {
            foreach (var window in _openedWindows)
            {
                window.Value.Close();
            }

            _openedWindows.Clear();
        }

        public enum WindowType
        {
            Start = 0,
            Game = 1,
            Victory = 2
        }
    }
}