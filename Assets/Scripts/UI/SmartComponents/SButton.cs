using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI.SmartComponents
{
    [RequireComponent(typeof(Button))]
    public class SButton : MonoBehaviour
    {
        private Button _button;
        public event Action<SButton> ButtonPressed;

        public void Initialize()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(OnButtonClicked);
        }


        private void OnButtonClicked()
        {
            ButtonPressed?.Invoke(this);
        }
    }
}