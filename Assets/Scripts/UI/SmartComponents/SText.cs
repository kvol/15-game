﻿using TMPro;
using UnityEngine;
using Utilities.Properties.Interfaces;

namespace UI.SmartComponents
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class SText : MonoBehaviour
    {
        private TextMeshProUGUI _text;

        public void Initialize(IProperty property)
        {
            _text = GetComponent<TextMeshProUGUI>();
            property.ValueChanged += OnPropertyValueChanged;
        }

        private void OnPropertyValueChanged(string newText)
        {
            _text.text = newText;
        }
    }
}