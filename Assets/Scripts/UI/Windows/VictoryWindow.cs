﻿using System;
using Core.Interfaces;
using UI.SmartComponents;
using UnityEngine;

namespace UI.Windows
{
    public class VictoryWindow : WindowBase
    {
        [SerializeField] private SButton _newGameButton;
        [SerializeField] private SText _gameTime;
        [SerializeField] private SText _gameMoves;
        [SerializeField] private SText _bestTime;
        [SerializeField] private SText _bestMoves;

        private Action _startNewGame;

        public void Initialize(Action startNewGame, IRecordsProperties recordsProperties)
        {
            _startNewGame = startNewGame;
            _newGameButton.Initialize();
            _gameTime.Initialize(recordsProperties.CurrentTimeProperty);
            _gameMoves.Initialize(recordsProperties.CurrentMovesProperty);
            _bestTime.Initialize(recordsProperties.BestTimeProperty);
            _bestMoves.Initialize(recordsProperties.BestMovesProperty);
        }

        public override void SubscribeAllActions()
        {
            _newGameButton.ButtonPressed += OnNewGameButtonPressed;
        }


        public override void UnsubscribeAllActions()
        {
            _newGameButton.ButtonPressed -= OnNewGameButtonPressed;
        }

        private void OnNewGameButtonPressed(SButton button)
        {
            _startNewGame.Invoke();
        }
    }
}