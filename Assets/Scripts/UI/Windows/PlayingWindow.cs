﻿using System;
using Core.Interfaces;
using UI.SmartComponents;
using UnityEngine;

namespace UI.Windows
{
    public class PlayingWindow : WindowBase
    {
        [SerializeField] private SButton _newGameButton;
        [SerializeField] private SText _timeText;
        [SerializeField] private SText _currentMoves;
        [SerializeField] private SText _bestMoves;
        private Action _startNewGame;

        public void Initialize(Action startNewGame, IRecordsProperties recordsProperties)
        {
            _startNewGame = startNewGame;
            _newGameButton.Initialize();
            _timeText.Initialize(recordsProperties.CurrentTimeProperty);
            _currentMoves.Initialize(recordsProperties.CurrentMovesProperty);
            _bestMoves.Initialize(recordsProperties.BestMovesProperty);
        }

        public override void SubscribeAllActions()
        {
            _newGameButton.ButtonPressed += OnNewGameButtonPressed;
        }

        public override void UnsubscribeAllActions()
        {
            _newGameButton.ButtonPressed -= OnNewGameButtonPressed;
        }

        private void OnNewGameButtonPressed(SButton button)
        {
            _startNewGame.Invoke();
        }
    }
}