﻿using System;
using System.Collections.Generic;
using UI.SmartComponents;
using UnityEngine;

namespace UI.Windows
{
    public class StartWindow : WindowBase
    {
        [SerializeField] private int _startingSize;
        [SerializeField] private List<SButton> _levelSizeButtons;
        private Action<int> _createNewGame;

        public void Initialize(Action<int> createNewGame)
        {
            _createNewGame = createNewGame;
            foreach (var button in _levelSizeButtons)
            {
                button.Initialize();
            }
        }

        public override void SubscribeAllActions()
        {
            foreach (var levelSizeButton in _levelSizeButtons)
            {
                levelSizeButton.ButtonPressed += OnStartButtonPressed;
            }
        }

        public override void UnsubscribeAllActions()
        {
            foreach (var levelSizeButton in _levelSizeButtons)
            {
                levelSizeButton.ButtonPressed -= OnStartButtonPressed;
            }
        }

        private void OnStartButtonPressed(SButton button)
        {
            var buttonNumber = _levelSizeButtons.IndexOf(button);
            _createNewGame.Invoke(_startingSize + buttonNumber);
        }
    }
}