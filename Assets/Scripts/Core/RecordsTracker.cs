﻿using System;
using System.Collections.Generic;
using Core.Interfaces;
using Model.Data;
using Utilities;
using Utilities.Properties;
using Utilities.Properties.Interfaces;

namespace Core
{
    public class RecordsTracker : IRecordsProperties
    {
        private const int MinimalGridSize = 3;

        private readonly Timer _timer = new Timer();
        private readonly Counter _movesCounter = new Counter();

        private List<int> _bestTimePerGridSize = new List<int>()
        {
            0, 0, 0, 0, 0
        };

        private List<int> _bestMovesPerGridSize = new List<int>()
        {
            0, 0, 0, 0, 0
        };

        private readonly Time _bestTime = new Time(0, 0);
        private readonly IntProperty _bestMovesCount = new IntProperty();

        private bool _isTracking;

        public IProperty CurrentTimeProperty => _timer.Time;
        public IProperty CurrentMovesProperty => _movesCounter.Count;

        public IProperty BestTimeProperty => _bestTime;
        public IProperty BestMovesProperty => _bestMovesCount;

        public GameRecordsData RecordsData =>
            new GameRecordsData(
                _timer.Time.TimeInSeconds,
                _movesCounter.Count.Value,
                _bestTimePerGridSize,
                _bestMovesPerGridSize);

        public void LoadRecords(GameRecordsData gameData)
        {
            
            _bestTimePerGridSize = new List<int>(gameData.RecordTime);
            _timer.Time.TimeInSeconds = gameData.CurrentTime;
            _bestMovesPerGridSize = new List<int>(gameData.RecordMoves);
            _movesCounter.Count.Value = gameData.CurrentMoves;
        }

        public void CellMoved()
        {
            if (_isTracking == false)
            {
                return;
            }

            _movesCounter.Tick();
        }

        public void Tick(float deltaTime)
        {
            if (_isTracking == false)
            {
                return;
            }

            _timer.Tick(deltaTime * 1000);
        }

        public void StartTracking(int gridSize)
        {
            int listNumber = gridSize - MinimalGridSize;
            _bestTime.TimeInSeconds = _bestTimePerGridSize[listNumber];
            _bestMovesCount.Value = _bestMovesPerGridSize[listNumber];
            _isTracking = true;
        }

        public void StopTracking()
        {
            _isTracking = false;
        }

        public void SaveResults()
        {
            _bestTime.TimeInSeconds = _bestTime.TimeInSeconds == 0
                ? _timer.Time.TimeInSeconds
                : Math.Min(_timer.Time.TimeInSeconds, _bestTime.TimeInSeconds);

            _bestMovesCount.Value = _bestMovesCount.Value == 0
                ? _movesCounter.Count.Value
                : Math.Min(_movesCounter.Count.Value, _bestMovesCount.Value);
        }

        public void ResetResults()
        {
            _timer.Reset();
            _movesCounter.Reset();
        }
    }
}