﻿using System.Collections.Generic;
using DG.Tweening;
using Model;
using Model.Data;
using UI;
using UnityEngine;
using Utilities;
using View;
using Time = UnityEngine.Time;

namespace Core
{
    public class GameManager : MonoBehaviour
    {
        [SerializeField] private GameViewController _gameViewController;

        // ReSharper disable once InconsistentNaming
        [SerializeField] private UIManager _UIManager;

        private GameModel _gameModel;

        private int _gridSize;

        private readonly GameStateSaver _gameSaver = new GameStateSaver();
        private readonly RecordsTracker _recordsTracker = new RecordsTracker();

        private bool _isGameRunning;

        private void Start()
        {
            _UIManager.Initialize(StartNewGame, CreateNewGame, _recordsTracker);
            GameData loadedData = _gameSaver.Load();
            if (loadedData == null)
            {
                StartNewGame();
            }
            else
            {
                if (loadedData.IsGameRunning)
                {
                    LoadCurrentGame(loadedData);
                }
                else
                {
                    StartNewGame();
                    LoadRecords(loadedData);
                }
            }
        }

        private void Update()
        {
            _recordsTracker.Tick(Time.deltaTime);
        }

        private void StartNewGame()
        {
            _recordsTracker.StopTracking();
            _UIManager.OpenWindow(UIManager.WindowType.Start);
        }

        private void LoadCurrentGame(GameData dataToLoad)
        {
            _gridSize = dataToLoad.GridStateData.GridSize;
            _gameModel = new GameModel(dataToLoad.GridStateData);
            _gameViewController.Initialize(dataToLoad.GridStateData);
            StartGame();
            LoadRecords(dataToLoad);
        }

        private void LoadRecords(GameData dataToLoad)
        {
            _recordsTracker.LoadRecords(dataToLoad.GameRecordsData);
        }

        private void CreateNewGame(int gridSize)
        {
            _gridSize = gridSize;

            _UIManager.CloseAllWindows();
            _UIManager.OpenWindow(UIManager.WindowType.Game);

            _gameViewController.CellClicked -= OnCellClicked;

            _gameModel = new GameModel(gridSize);
            _gameViewController.Initialize(gridSize);

            var shufflingMoves = _gameModel.Shuffle(gridSize * gridSize * 2);
            DOVirtual.DelayedCall(0.5f,
                () => _gameViewController.ShufflingAnimation(shufflingMoves));
            _gameViewController.AnimationEnded += StartGame;
        }

        private void StartGame()
        {
            _isGameRunning = true;
            _gameViewController.AnimationEnded -= StartGame;

            _recordsTracker.ResetResults();

            _gameViewController.CellClicked += StartTimer;

            _gameViewController.CellClicked += OnCellClicked;
        }

        private void StartTimer(Vector2Int vector)
        {
            _recordsTracker.StartTracking(_gridSize);
            _gameViewController.CellClicked -= StartTimer;
        }

        private void OnCellClicked(Vector2Int cellPosition)
        {
            Queue<MoveVector2Int> movementList = _gameModel.TryMove(cellPosition);

            if (movementList.Count < 1)
            {
                return;
            }

            SaveGame();
            _recordsTracker.CellMoved();
            _gameViewController.AddMovementToQueue(movementList);
            bool isGridSolved = _gameModel.IsGridSolved();

            if (isGridSolved)
            {
                _gameViewController.AnimationEnded += EndGame;
            }
        }

        private void EndGame()
        {
            _gameViewController.AnimationEnded -= EndGame;
            _isGameRunning = false;
            _recordsTracker.StopTracking();
            _recordsTracker.SaveResults();
            _UIManager.CloseAllWindows();
            _UIManager.OpenWindow(UIManager.WindowType.Victory);
            SaveGame();
        }

        private void OnDestroy()
        {
            SaveGame();
        }

        private void SaveGame()
        {
            _gameSaver.Save(new GameData(_gameModel?.GameModelState, _recordsTracker.RecordsData, _isGameRunning));
        }
    }
}