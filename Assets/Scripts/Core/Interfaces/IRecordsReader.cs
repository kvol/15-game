﻿using Utilities.Properties.Interfaces;

namespace Core.Interfaces
{
    public interface IRecordsProperties
    {
        IProperty CurrentTimeProperty { get; }
        IProperty CurrentMovesProperty { get; }
        IProperty BestTimeProperty { get; }
        IProperty BestMovesProperty { get; }
    }
}