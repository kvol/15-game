﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Model.Data;
using UnityEngine;

namespace Core
{
    public class GameStateSaver
    {
        private string _savingFolder;

        public void Save(GameData dataToSave)
        {
            using (FileStream file = File.Create(Application.persistentDataPath + "/gamesave.save"))
            {
                BinaryFormatter bf = new BinaryFormatter();
                bf.Serialize(file, dataToSave);
            }
        }

        public GameData Load()
        {
            GameData loadedData;
            FileStream file = null;
            try
            {
                file = File.Open(Application.persistentDataPath + "/gamesave.save", FileMode.Open);
                BinaryFormatter bf = new BinaryFormatter();
                loadedData = (GameData) bf.Deserialize(file);
                file.Dispose();
            }
            catch
            {
                loadedData = null;
            }
            finally
            {
                file?.Dispose();
            }

            return loadedData;
        }
    }
}